---
title: VOA
layout: default
parent: Feature Guides
has_children: true
nav_order: 10
permalink: /voa/
---



# VOXL Vision Hub Visual Obstacle Avoidance
{: .no_toc }


The `voxl-vision-hub` service supports consuming a point cloud from the [Depth from Stereo](/voxl-dfs-server/) service and processing it into an obstacle_distance mavlink message for use by PX4 and ArduPilot. voxl-vision-hub will use its history of VIO data to correct for the motion of the stereo cameras over time during the depth processing. Without VIO enabled it will just use PX4's reported attitude to correct for orientation.

<div class="video-container">
    <iframe src="https://www.youtube.com/embed/tfI4L5Tj_oc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>


## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## VOXL Configuration

 - VOXL needs to be equipped with calibrated stereo cameras or a PMD TOF to use this feature. If you purchased a [Flight Deck](/flight-deck/) or [M500 Developer Drone](/m500/) then the cameras should have been calibrated from the factory.

 - Next, make sure the [Depth from Stereo](/voxl-dfs-server/) server is running if you are using DFS for VOA. This can be done by looking at [voxl-inspect-services](/voxl-inspect-services/). It is disabled by default by [voxl-configure-mpa](/voxl-configure-mpa/) so you will likely need to enable it with `systemctl enable voxl-dfs-server`.

 - Make sure the `"en_voa"` field in `/etc/modalai/voxl-vision-hub.conf` file is set to `true` which is is by default. This option is there mostly to enable you to disable voxl-vision-hub from consuming DFS data if you wish to use DFS for something other than PX4's Collision Prevention.

 - Check that point cloud data is being published from voxl-dfs-server using the voxl-inspect-points[/voxl-inspect-points/) tool.

 - Make sure you have a correct extrinsic relation from the `body` frame to the `stereo` or `tof` frame in `/etc/modalai/extrinsics.conf`. For ModalAI reference drones this is set up automatically with [voxl-configure-extrinsics](/voxl-configure-extrinsics/) and by extension the higher level [voxl-configure-mpa](/voxl-configure-mpa/) tools. This can be checked with the [voxl-inspect-extrinsics](/voxl-inspect-extrinsics/) tool.


## PX4 Configuration

The above only enables voxl-vision-hub to send obstacle data to PX4. PX4 must then be configured to use this data. For full information see the PX4 docs here: [https://docs.px4.io/main/en/computer_vision/collision_prevention.html](https://docs.px4.io/main/en/computer_vision/collision_prevention.html)

We suggest the following parameters for testing this indoors:

```
CP_DIST: 1.5
CP_DELAY: 0.0
CP_GUIDE_ANG: 0.0
CP_GO_NO_DATA: 1
```

The delay can safely be set to 0 since we do accurate timestamping and delay compensation inside voxl-vision-hub. Increasing the delay over 0 will only cause twitchiness and unpredictable performance.


## Debugging Collision Prevention

### voxl-vision-hub

voxl-vision-hub can be started with the following argument to print obstacle data to the screen before it is sent to PX4.

```
-s, --debug_stereo          print detected stereo obstacles as linescan points
```

The parameter `en_send_voa_to_qgc` can be set to `true` in `/etc/modalai/voxl-vision-hub.conf` to send the same mavlink OBSTACLE_DISTANCE messages to qgroundControl that are are sent to PX4. They can then be plotted with qGroundControl's mavlink_inspector tool.

#### Logging VOA Data

Use the following command to log VOA data, this is helpful for offline analysis

```voxl-logger --cam stereo --cam dfs_disparity```

### voxl-portal to View Depth Map

The [voxl-portal Cameras](https://docs.modalai.com/voxl-portal-cameras/) page can be used to view image sensor data, including processed stereo data. The following data is useful:
* Stereo is the data that goes into [voxl-dfs-server](https://docs.modalai.com/voxl-dfs-server) - [code](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-dfs-server)
* DFS Disparity Scaled is the data that goes into the collision prevention algorithm.
* voa_pc is the output of the VOA algorithm and what is sent on the obstacle_distance message [code](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-vision-hub/-/blob/master/src/voa_manager.c#L202)

### Calibrating Stereo Image Sensors

The stereo algorithm is highly sensitive to extrinsic calibration. If the sensors have been moved at all, it is likely they need to be re-calibrated. Details of how to calibrate stereo image sensors can be found [here](https://docs.modalai.com/calibrate-cameras/#stereo-calibration-process) using [voxl-portal](https://docs.modalai.com/voxl-portal).

## Frames of Reference Used

The geometry module inside voxl-vision-hub keeps track of the following frames of reference.

![px4_chart-geometry_overview](/images/voxl-sdk/voxl-vision-hub/px4_chart-geometry_overview.png)


voxl-vision-hub maintains a short history of visual odometry data to compensate for the time delay when computing the depth-from-stereo data as well as the motion and tilt of the drone body.

![px4_chart-stereo_frames](/images/voxl-sdk/voxl-vision-px4/px4_chart-stereo_frames.png)
