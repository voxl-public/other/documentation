---
title: ELRS
layout: default
parent: Feature Guides
has_children: true
nav_order: 47
permalink: /voxl-elrs/
---

# voxl-elrs

<a class="gl-mr-3" data-qa-link-url="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-elrs/-/commits/master" data-qa-selector="badge_image_link" href="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-elrs/-/commits/master" rel="noopener noreferrer" target="_blank"><img alt="Project badge" aria-hidden="" class="project-badge" src="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-elrs/badges/master/pipeline.svg?key_text=Master+Pipeline&amp;key_width=100&amp;ignore_skipped=true"></a> <a class="gl-mr-3" data-qa-link-url="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-elrs/-/commits/dev" data-qa-selector="badge_image_link" href="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-elrs/-/commits/dev" rel="noopener noreferrer" target="_blank"><img alt="Project badge" aria-hidden="" class="project-badge" src="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-elrs/badges/dev/pipeline.svg?key_text=Dev+Pipeline&amp;key_width=85&amp;ignore_skipped=true"></a> <a class="gl-mr-3" data-qa-link-url="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-elrs/-/releases" data-qa-selector="badge_image_link" href="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-elrs/-/releases" rel="noopener noreferrer" target="_blank"><img alt="Project badge" aria-hidden="" class="project-badge" src="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-elrs/-/badges/release.svg?key_text=Latest Release&amp;key_width=100"></a>

## Overview

<i class="fab fa-gitlab" style="color: #e66100;"></i>
[voxl-elrs source code](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-elrs)
[ExpressLRS Project Page](https://www.expresslrs.org/faq/)

### What is ExpressLRS?
ExpressLRS (ELRS) aims to provide the best completely open, high refresh radio control link while maintaining a maximum achievable range at that rate with low latency. Vast support of hardware in both 900 MHz and 2.4 GHz frequencies using the crossfire protocol.

### What is voxl-elrs?
`voxl-elrs` is an application introduced in VOXL SDK 1.0.0 which gives a user the ability to flash a receiver or transmitter, put a receiver into bind mode, and get device information from a receiver.

There is a large array of ELRS compatible receivers and transmitters on the market and we haven't tested them all, but below is a list of some that we know `voxl-elrs` supports.

In theory, `voxl-elrs` supports any receiver that is compatible with ELRS, however some functionalities such as setting a receiver into bind mode or assigning pwm channels depends on ModalAI firmware modifications. Here is a list of currently supported receivers and transmitters:


| Receiver            | Frequency | Latest FW | voxl-elrs |
|:-------------------:|:---------:|:---------:|:---------:|
| BetaFPV Nano RX     |  915MHz   |  3.2.1.2  |   0.0.7+  |
| FrSky R9MM/Mini-OTA |  915MHz   |  3.2.1.3  |   0.1.0+  |
| ModalAI M0184       |  915MHz   |  3.5.3.3  |   0.3.0+  |

| Transmitter         | Frequency | Latest FW | voxl-elrs |
|:-------------------:|:---------:|:---------:|:---------:|
| iFlight Commando 8  |  915MHz   |  3.0.1    |   0.0.7+  |
| ModlaAI M0193       |  915MHz   |  3.5.3.3  |   0.3.2+  |

### Installation

`voxl-elrs` can be installed on the VOXL platform or on any Debian distribution with access to the [ModalAI package repository](/configure-pkg-manager/#adding-the-modal-ai-package-repository).

```
voxl2:/$ sudo apt install voxl-elrs
```


## Binding Procedure

{% include youtubePlayer.html id="7OwGS-kcFVg" %}

**Note: Ensure that voxl-elrs version is 0.0.8+ and that both the receiver and transmitter are on ELRS version 3.0.0+!** 
## Usage
Currently, `voxl-elrs` does not require any configuration in order to get started. Usage is as follows:

``` 
voxl2:/$ voxl-elrs -h
Description: Flash a RX or TX with ELRS firmware.

usage: voxl-elrs [-h] [-d DEVICE] [-w] [-q] [-b] [-B] [-u] [-l] [-v] [-s] [-p SETID] [-n] [command ...]

positional arguments:
  command               Command to run (optional) [help, bind, unbind, update, info, setid, configure]

options:
  -h, --help            Show this help message and exit
  -d DEVICE, --device DEVICE
                        Serial port
  -w, --wizard          Activate interactive wizard
  -q, --quiet           Quiet mode, don't print debug msgs
  -b, --bind            Put receiver into bind mode
  -B, --betaflight      Put betaflight flight controller into passthrough mode
  -u, --unbind          Unbind a bound transmitter
  -l, --bootloader      Put receiver into bootloader mode
  -v, --version         Get receiver version info
  -s, --scan            Scan current firmware and update if <=3.2.1
  -p SETID, --setid SETID
                        Sets the binding phrase of a connected receiver
  -n, --noscan          Disables auto-detection of the connected receiver

```

If you run `voxl-elrs` without any commands (or flags which are aliases for commands such as "--bind") the wizard will start automatically and attempt to auto-detect a connected receiver.

## Wiring Guide
### BetaFPV Nano RX
![BetaFPV Nano RX Wiring Diagram](/images/voxl-sdk/voxl-elrs/BetaFPV_wiring.png)

### FrSky R9MM/Mini-OTA
![FrSky R9Mini Wiring Diagram](/images/voxl-sdk/voxl-elrs/FrSky_R9Mini_wiring.png)

## Manually flashing a receiver

- In order to manually flash an ELRS receiver, make sure that the receiver is not in bootloader mode and that it is not connected to a transmitter. These conditions are necessary so we can get device information from the receiver.  

- On host computer adb shell onto the voxl2 or ssh:    

``` bash
adb shell
OR
ssh root@<voxl2 ip> 
```

**Note: The default ssh password is oelinux123** 

- Now, on the voxl2 use the `voxl-elrs` wizard and select the "Flash Receiver" option:

``` bash
voxl2:/$ voxl-elrs -w
Please choose an option below

==============================

 1.	Bind Receiver
 2.	Unbind Receiver
 3.	Set Receiver ID
 4.	Modify PWM Settings
 5.	Restart Receiver
 6.	Recover Receiver
 7.	Flash Receiver
 8.	Flash Transmitter
 9.	Start Configuration Wizard
> 7
```

- Choose auto-update if you want to use the latest version and the wizard auto-detected your receiver in the first step.

- Otherwise, manually choose your receiver and firmware version for the first installation.

```bash
Please choose an option below

==============================

 1.	Auto-Update
 2.	Choose Version

 0.	Back
> 2
```

- Select which firmware binary to flash:    

```bash 
Select the firmware to flash:

 1.	ModalAI : M0193 | ELRS v3.5.3.3 | Local: True
 2.	ModalAI : M0184 | ELRS v3.5.3.3 | Local: True
 3.	betafpv : BETAFPV 900MHz RX | ELRS v3.2.1.2 | Local: True
 4.	frsky : FrSky R9 Mini | ELRS v3.3.2.1 | Local: True
 5.	ModalAI : M0184 | ELRS v3.3.2.1 | Local: True
 6.	ModalAI : M0184 | ELRS v3.5.3.2 | Local: True

 0.	Back
> 2
```

- The receiver you selected will now be flashed with the latest firmware!    

## Recovering a bricked receiver
- Manually place the receiver into bootloader mode by holding down the boot button while power cycling the receiver.    
![Bootloader button](/images/voxl-sdk/voxl-elrs/elrs_recover_1.jpg)
- On host computer adb shell onto the voxl2 or ssh:    

``` bash
adb shell
OR
ssh root@<voxl2 ip> 
```

- Now, on the voxl2 use the `voxl-elrs` wizard and select the "Recover Receiver" option:

``` bash
voxl2:/$ voxl-elrs -w
Please choose an option below

==============================

 1.	Bind Receiver
 2.	Unbind Receiver
 3.	Set Receiver ID
 4.	Modify PWM Settings
 5.	Restart Receiver
 6.	Recover Receiver
 7.	Flash Receiver
 8.	Flash Transmitter
 9.	Start Configuration Wizard
> 6
```

- This only works for ESP-32 and ESP-8266 based receivers, please confirm that your receiver meets these criteria:    

```bash 
Receiver recovery is only for ESP-based receivers which are unable to be flashed normally.
This will wipe all data from your receiver before prompting you to flash new software.
Enter 'y' to confirm.
> y
```

- Follow the instructions given and if the recovery is successful you should see an output like the image below    
![Bootloader button](/images/voxl-sdk/voxl-elrs/elrs_recover_2.png)

- If the recovery process fails, double check that the receiver is actually in bootloader mode. Make sure that you hold the bootloader button down for a couple seconds after power cycling the receiver.


## PWM Configuration

### PWM Capable Receivers    

The R9MM/Mini-OTA is used as an example of enabling PWM on an STM32 based ELRS receiver (there is no native ELRS support for PWM on STM32 based receivers in ELRS firmware version 3.2.1.X). This receiver is supported by voxl-elrs but not necessarily sold by ModalAI.

| Receiver            | Frequency | PWM Channels | Latest FW | voxl-elrs |
|:-------------------:|:---------:|:------------:|:---------:|:---------:|
| FrSky R9MM/Mini-OTA |  915MHz   |       3      |  3.2.1.3  |  0.1.1+   |    
| ModalAI M0184       |  915MHz   |       4      |  3.5.3.3  |  0.3.5+   |    
| ModalAI M0193       |  915MHz   |       4      |  3.5.3.3  |  0.3.5+   |    

#### FrSky R9MM/Mini-OTA PWM outputs
![FrSky R9MM/Mini-OTA PWM Channels](/images/voxl-sdk/voxl-elrs/FrSky_R9Mini_PWM.png)

### Setting up PWM Channels

In the voxl-elrs wizard, there is an option to `Modify PWM Settings`. After choosing this option, the following prompt appears:

```

Please choose an option below

==============================

 1.	Set PWM Value
 2.	Set PWM Channel Mapping

 0.	Back
> 2
```

To set up PWM channels, choose option 2. The next option will prompt you for the pin to set on your receiver. The pins are numbered from 0 to 3 on receivers with 4 PWM pins.

```
Set the PWM parameters:
Pin to set (0-3) > 0
```

Then it will prompt you for the input channel from the transmitter. The channels are numbered from 1 to 255 (though no transmitters actually send that many channels, the number of available channels depends on your handset and ELRS configuration).

```
Set the PWM parameters:
Pin to set (0-3) > 0
Input Channel (1-255) > 7
```

It will then take you back to the previous screen so you can continue assigning all of the pins.

## Binding phrase

The menu item `Set Receiver ID` can be used to set a unique binding phrase for the connected receiver. After selecting that option, you will see the following prompt:

```
Enter the binding phrase or press enter to cancel
> 
```

Press enter after entering your binding phrase and it will assign that phrase to your receiver.

## Manage ELRS from Ubuntu host

As noted in the Installation section, `voxl-elrs` can be installed on any Debian/Ubuntu machine. By default it will attempt to use the serial port `/dev/ttyUSB0`. If your receiver is connected to a different port, use the optional flag `-d {port}` to specify it. Otherwise, `voxl-elrs` on Ubuntu will perform identically to `voxl-elrs` on a Voxl.

## Betaflight Passthrough

For receivers connected to a Betaflight flight controller, `voxl-elrs` can still be used to manage the receiver through the use of Betaflight passthrough. The flag `-B` or `--betaflight` will attept to setup betaflight passthrough on the port specified with `-d`. In order for this to be successful, the betaflight configurator tool must be disconnected from the flight controller. The passthrough setup will persist until the flight controller is power cycled, so subsequent calls to `voxl-elrs` do not need the `-B` flag, though it is still recommended in case the flight controller reboots.

## Configuration Wizard

To make managing a fleet of voxl-connected ELRS receivers easier, the `voxl-elrs` configuration wizard can be used to declare the desired firmware version, PWM mappings, and binding phrase of the connected receiver and apply these settings all at once.
The configuration wizard provides an interface for editing the file `/etc/modalai/voxl-elrs.conf`, which can be manually edited and copied across devices.

### Configuration Fields

#### PWM

The PWM pins can be mapped to channels or disabled in the configuration menu.

```
Set all values below:

 1.	PWM 0 : Enabled, Ch 7, Mode Default, Failsafe 50
 2.	PWM 1 : Enabled, Ch 8, Mode Default, Failsafe 50
 3.	PWM 2 : Enabled, Ch 9, Mode Default, Failsafe 50
 4.	PWM 3 : Disabled

 0.	Back
> 
```

#### Binding Phrase

A binding phrase can be specified or left empty if traditional binding is preferred.

#### Receiver Model
A configuration can also specify the receiver type. If the receiver specified in this field isn't detected when uploading a configuration, the configuration procedure will fail.

#### Multiple Configurations
Multiple configurations can exist in this file, but the one marked as "default" is the one which will be uploaded when running `voxl-elrs configure`.

### Applying the default configuration

A quick command is available to apply the selected default configuration `voxl-elrs configure` will ensure that your receiver firmware, PWM, and binding phrase match your configured values.

### Setting up the systemd service

To keep a fleet of ELRS receivers up to date and configured correctly, a systemd service script is included in voxl-elrs which will automatically run `voxl-elrs configure` on a system startup.
This will keep your ELRS receivers up to date automatically (if configured to use the latest version), and ensure the correct binding phrase and PWM mappings are always set.
Make sure not to enable this script if you make any manual changes to your receiver without changing the default configuration; otherwise, your changes will be overridden.

To enable the startup script, run the command `systemctl enable voxl-elrs-startup.service`.


## Tips
- In order for a receiver and transmitter to bind, they need to be on the same major version of ELRS. For voxl-elrs, this means that **both the transmitter and receiver need to be on ELRS version 3.0.0+**.
- The recommended usage of this tool is to use the interactive wizard, by either using `voxl-elrs -w` or `voxl-elrs --wizard` and then following the prompt.
- If making your own custom modifications to the receiver firmware, be careful using the `voxl-elrs --scan` function. If `voxl-elrs` detects a firmware version that doesn't match the latest for that version of `voxl-elrs`, it will overwrite the firmware you have currently loaded.
- You can determine the state of the receiver by inspecting the LED on your receiver and comparing to the information on [the ELRS LED Status page](https://www.expresslrs.org/quick-start/led-status/).
- Useful information regarding all things ExpressLRS (ELRS) can be found on the [ELRS quickstart page](https://www.expresslrs.org/quick-start/getting-started/).

