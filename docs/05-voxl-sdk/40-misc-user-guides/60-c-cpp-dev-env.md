---
title: Setting up a C/C++ Dev Environment
layout: default
parent: Misc. User Guides
has_children: true
nav_order: 60
permalink: /cpp-dev-environment/
---

# Setting up a C/C++ Dev Environment
{: .no_toc }

## Introduction

A large share of the ModalAI software stack is written in C/C++ and so if you intend to develop applications for VOXL there's a decent chance you'll need to use the language. However, C/C++ is a complicated language with a very dated build system, and as such editors like VSCode can struggle to understand these projects out of the box leading to a ton of red squiggles and errors while trying to work on the project. In addition to being visually distracting, this also massively limits the functionality of these modern IDEs: features like code completion function lookup/referencing will all mostly struggle to work. In this document, I'll outline the main ways you can set up a development environment for C/C++ to make working with the VOXL SDK a breeze.

## Setup
Start by downloading VSCode from their [downloads page](https://code.visualstudio.com/download). Also make sure to install the [Dev Containers](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) and [C/C++](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools) extensions as both will be needed to get the environment set up correctly. Also make sure to [set up voxl-cross](https://docs.modalai.com/voxl-docker-and-cross-installation/) so that you'll have the container to build in.

## Process
* Open a terminal and launch the `voxl-cross` container using `voxl-docker -i voxl-cross`. Make sure to launch this in a directory above or at the project you intend to work on (e.g. don't runt his command in `~/git/voxl-portal/src` if you intend to work on `~/git/voxl-portal/`).
* Open VSCode and `CTRL+Shift+P` to open and find the option `Dev Containers: Attach to Running Container...`. This will prompt you to pick a running container, you should pick the one corresponding to the `voxl-cross` instance you just spun up. From here you can use `File > Open Folder` to open up the project you want to work on.
* Open up `CMakeLists.txt` at the root of the project you're working on and look for the line `set(CMAKE_EXPORT_COMPILE_COMMANDS ON)`. If it isn't already in the `CMakeLists.txt` file, add it! This line generates a `.json` file that tells the `C/C++` extension how to interpret our project. 
* Go back to the terminal with `voxl-cross` in it and navigate to your project. Then run `./clean.sh`, `./install_build_deps.sh qrb5165 dev` and finally `./build.sh`. Now check the `build64` directory that was created in your project. Inside there should be a file called `compile_commands.json`.
* At the root of your project, run `mkdir .vscode && cd .vscode && touch c_cpp_properties.json`, then open this `c_cpp_properties.json` file. Paste the following content:
```
{
    "configurations": [
        {
            "name": "Linux",
            "compileCommands": "${workspaceFolder}/build64/compile_commands.json",
            "cStandard": "c11",
            "cppStandard": "c++17",
            "intelliSenseMode": "clang-x64"
        }
    ],
    "version": 4
}
```

And you should be set! A few things to keep in mind:
* Every time you shut down `voxl-cross` and restart it, you'll need to re-run the `./install_build_deps qrb5165 dev` command to re-install the build dependencies for the project 
* These setup steps only apply to this project. If you go to another project you'll need to do the same process as above. Fortunately you can just copy your `.vscode` dir around for easier access.
* If you change to a 32-bit build system the link to the compile commands file may change. The command `find . -name "compile_commands.json"` can help you get the right path to that file so you can update your `c_cpp_properties.json`. 


