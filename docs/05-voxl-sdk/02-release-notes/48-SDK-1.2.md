---
title: SDK 1.2 Release Notes
layout: default
parent: VOXL SDK Release Notes
has_children: false
nav_order: 48
permalink: /sdk-1.2-release-notes/
---

# SDK 1.2.X Release Notes

{: .no_toc }

Available now at [https://developer.modalai.com/asset/](https://developer.modalai.com/asset/)

SDK 1.2 Support Matrix

| Platform            | PCB Targets                        | Support Status                                         |
|---------------------|------------------------------------|--------------------------------------------------------|
| VOXL 2              | M0054-1, M0054-2, M0154-1, M0154-2 | <img src="/icons/circle-check-big.svg">                |
| VOXL 2 Mini         | M0104-1                            | <img src="/icons/circle-alert.svg">                    |
| VOXL                | M0006-2                            | <img src="/icons/circle-check-big.svg">                |
| VOXL Flight         | M0019-2                            | <img src="/icons/circle-check-big.svg">                |
| Qualcomm Flight RB5 | M0052-2                            | <img src="/icons/link-2-off.svg">                      |
| Flight Core v2      | M0087-1                            | <img src="/icons/circle-check-big.svg">                |
| Flight Core v1      | M0018-1                            | <img src="/icons/circle-check-big.svg">                |

The SDK-1.2.0 release on the QRB5165 platform use the 1.7.6 system image.  See [here](/voxl2-voxl2-mini-system-image/) for system image release notes.

Note: Updated Flight Core firmware is bundled in the APQ8096 SDK package and also available in the voxl-packages package repo here: [http://voxl-packages.modalai.com/dists/](http://voxl-packages.modalai.com/dists/)

Don't forget to flash your flight core through QGC ([instructions here](/flight-core-v2-firmware/)) with the new firmware.



# Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

# New Features in SDK 1.2.X

- New Camera Support

    + liow2 ToF support
    + Improved calibration for ov9782 tracking cameras
    
- PX4

    + Support for VIO waypoint missions without GPS

- Other

    + voxl-gpio improvements, now allows gpio reads on output
    + M0154 support in voxl-bind-spektrum
    + VOXL2: Ghost RC support
    + Start and stop options for voxl-replay, support for RAW16 images
    + Mission start delay option
    + New page in voxl-portal to download systemd service journal logs
    + Allow second camera in stereo pair to be used by voxl-qvio-server
    

# SDK 1.2.0 Package List and Changelog

## SDK 1.2.0 Package List

Release date: April 15, 2024

Package list and changes from SDK-1.1.3 to SDK-1.2.0

| Package                                                                                                   | Version                         | APQ8096 | QRB5165 |
|-:---------------------------------------------------------------------------------------------------------|-:-:-----------------------------|-:-:-----|-:-:-----|
| [apq8096-dfs-server](https://gitlab.com/voxl-public/voxl-sdk/services/apq8096-dfs-server)                 | 0.3.1 (unchanged)               | ✅       |         |
| [apq8096-imu-server](https://gitlab.com/voxl-public/voxl-sdk/services/apq8096-imu-server)                 | 1.1.0 (unchanged)               | ✅       |         |
| [apq8096-libpng](https://gitlab.com/voxl-public/voxl-sdk/third-party/apq8096-libpng)                      | 1.6.38-1 (unchanged)            | ✅       |         |
| [apq8096-rangefinder-server](https://gitlab.com/voxl-public/voxl-sdk/services/apq8096-rangefinder-server) | 0.1.3 (unchanged)               | ✅       |         |
| [apq8096-system-tweaks](https://gitlab.com/voxl-public/voxl-sdk/utilities/apq8096-system-tweaks)          | 0.2.3 (unchanged)               | ✅       |         |
| [apq8096-tflite](https://gitlab.com/voxl-public/voxl-sdk/third-party/apq8096-tflite)                      | 2.8.3-1 (unchanged)             | ✅       |         |
| [libapq8096-io](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libapq8096-io)                          | 0.6.0 (unchanged)               | ✅       |         |
| [libfc-sensor](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libfc-sensor)                            | 1.0.5 (unchanged)               |         | ✅       |
| [libmodal-cv](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-cv)                              | 0.4.0 (unchanged)               | ✅       | ✅       |
| [libmodal-exposure](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-exposure)                  | 0.1.0 (unchanged)               | ✅       | ✅       |
| [libmodal-journal](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-journal)                    | 0.2.2 (unchanged)               | ✅       | ✅       |
| [libmodal-json](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-json)                          | 0.4.3 (unchanged)               | ✅       | ✅       |
| [libmodal-pipe](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-pipe)                          | 2.9.2 --> 2.10.0                | ✅       | ✅       |
| [libqrb5165-io](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libqrb5165-io)                          | 0.4.2 --> 0.4.5                 |         | ✅       |
| [librc-math](https://gitlab.com/voxl-public/voxl-sdk/core-libs/librc-math)                                | 1.4.2 (unchanged)               | ✅       | ✅       |
| [libvoxl-cci-direct](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libvoxl-cci-direct)                | 0.2.1 (unchanged)               | ✅       | ✅       |
| [libvoxl-cutils](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libvoxl-cutils)                        | 0.1.1 (unchanged)               | ✅       | ✅       |
| [modalai-slpi](https://gitlab.com/voxl-public/voxl-sdk/utilities/modalai-slpi)                            | 1.1.12 (unchanged)              |         | ✅       |
| [qrb5165-dfs-server](https://gitlab.com/voxl-public/voxl-sdk/services/qrb5165-dfs-server)                 | 0.2.0 (unchanged)               |         | ✅       |
| [qrb5165-imu-server](https://gitlab.com/voxl-public/voxl-sdk/services/qrb5165-imu-server)                 | 1.0.1 (unchanged)               |         | ✅       |
| [qrb5165-rangefinder-server](https://gitlab.com/voxl-public/voxl-sdk/services/qrb5165-rangefinder-server) | 0.1.1 (unchanged)               |         | ✅       |
| [qrb5165-system-tweaks](https://gitlab.com/voxl-public/voxl-sdk/utilities/qrb5165-system-tweaks)          | 0.2.5 --> 0.2.6                 |         | ✅       |
| [qrb5165-tflite](https://gitlab.com/voxl-public/voxl-sdk/third-party/qrb5165-tflite)                      | 2.8.0-2 (unchanged)             |         | ✅       |
| [voxl-bind-spektrum](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-bind-spektrum)                | 0.1.0 --> 0.1.1                 |         | ✅       |
| [voxl-boost](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-boost)                              | 1.65.0 (unchanged)              | ✅       |         |
| [voxl-camera-calibration](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-camera-calibration)      | 0.5.3 --> 0.5.4                 | ✅       | ✅       |
| [voxl-camera-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-camera-server)                 | 1.8.9 --> 1.9.1                 | ✅       | ✅       |
| [voxl-ceres-solver](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-ceres-solver)                | 2:1.14.0-10 (unchanged)         | ✅       | ✅       |
| [voxl-configurator](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-configurator)                  | 0.5.2 --> 0.5.6                 | ✅       | ✅       |
| [voxl-cpu-monitor](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-cpu-monitor)                     | 0.4.7 --> 0.4.8                 | ✅       | ✅       |
| [voxl-docker-support](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-docker-support)              | 1.3.0 --> 1.3.1                 | ✅       | ✅       |
| [voxl-elrs](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-elrs)                                  | 0.1.3 (unchanged)               |         | ✅       |
| [voxl-esc](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-esc)                                    | 1.4.0 (unchanged)               |         | ✅       |
| [voxl-feature-tracker](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-feature-tracker)             | 0.3.2 (unchanged)               |         | ✅       |
| [voxl-flow-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-flow-server)                     | 0.3.3 (unchanged)               |         | ✅       |
| [voxl-gphoto2-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-gphoto2-server)               | 0.0.10 (unchanged)              | ✅       | ✅       |
| [voxl-jpeg-turbo](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-jpeg-turbo)                    | 2.1.3-5 (unchanged)             | ✅       | ✅       |
| [voxl-lepton-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-lepton-server)                 | 1.2.0 (unchanged)               | ✅       | ✅       |
| [voxl-libgphoto2](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-libgphoto2)                    | 0.0.4 (unchanged)               | ✅       | ✅       |
| [voxl-libuvc](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-libuvc)                            | 1.0.7 (unchanged)               | ✅       | ✅       |
| [voxl-logger](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-logger)                              | 0.3.5 --> 0.4.0                 | ✅       | ✅       |
| [voxl-mapper](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-mapper)                               | 0.2.0 (unchanged)               | ✅       | ✅       |
| [voxl-mavcam-manager](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-mavcam-manager)               | 0.5.3 (unchanged)               | ✅       | ✅       |
| [voxl-mavlink-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-mavlink-server)               | 1.3.2 --> 1.4.0                 | ✅       | ✅       |
| [voxl-modem](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-modem)                                | 1.0.8 --> 1.0.9                 | ✅       | ✅       |
| [voxl-mongoose](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-mongoose)                        | 7.7.0-1 (unchanged)             | ✅       | ✅       |
| [voxl-mpa-tools](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools)                        | 1.1.3 --> 1.1.5                 | ✅       | ✅       |
| [voxl-mpa-to-ros](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-to-ros)                      | 0.3.7 (unchanged)               | ✅       | ✅       |
| [voxl-mpa-to-ros2](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-to-ros2)                    | 0.0.2 (unchanged)               |         | ✅       |
| [voxl-neopixel-manager](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-neopixel-manager)          | 0.0.3 (unchanged)               | ✅       | ✅       |
| [voxl-nlopt](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-nlopt)                              | 2.5.0-4 (unchanged)             | ✅       | ✅       |
| [voxl-opencv](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-opencv)                            | 4.5.5-2 (unchanged)             | ✅       | ✅       |
| [voxl-open-vins](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-open-vins)                      | 0.4.4 (unchanged)               |         | ✅       |
| [voxl-open-vins-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-open-vins-server)           | 0.2.18 (unchanged)              |         | ✅       |
| [voxl-portal](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-portal)                               | 0.6.3 --> 0.6.5                 | ✅       | ✅       |
| [voxl-px4](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-px4)                                     | 1.14.0-2.0.63 --> 1.14.0-2.0.68 |         | ✅       |
| [voxl-px4-imu-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-px4-imu-server)               | 0.1.2 (unchanged)               |         | ✅       |
| [voxl-px4-params](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-px4-params)                      | 0.3.3 --> 0.3.8                 |         | ✅       |
| [voxl-qvio-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-qvio-server)                     | 1.0.0 --> 1.0.2                 | ✅       | ✅       |
| [voxl-remote-id](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-remote-id)                         | 0.0.9 (unchanged)               |         | ✅       |
| [voxl-ros2-foxy](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-ros2-foxy)                      | 0.0.1 (unchanged)               |         | ✅       |
| [voxl-streamer](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-streamer)                           | 0.7.4 (unchanged)               | ✅       | ✅       |
| [voxl-tag-detector](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-tag-detector)                   | 0.0.4 (unchanged)               | ✅       | ✅       |
| [voxl-tflite-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-tflite-server)                 | 0.3.1 --> 0.3.2                 | ✅       | ✅       |
| [voxl-utils](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-utils)                                | 1.3.8 --> 1.3.9                 | ✅       | ✅       |
| [voxl-uvc-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-uvc-server)                       | 0.1.6 (unchanged)               | ✅       | ✅       |
| [voxl-vision-hub](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-vision-hub)                       | 1.7.3 --> 1.7.4                 | ✅       | ✅       |
| [voxl-voxblox](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-voxblox)                          | 1.1.5 (unchanged)               | ✅       | ✅       |
| [voxl-vpn](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-vpn)                                    | 0.0.6 (unchanged)               | ✅       |         |



## SDK 1.2.0 Full Changelog (from SDK 1.1.3)

```
libmodal-pipe  (2.9.2 to 2.10.0)
2.10.0
    * add new tof2_data_t type for new PMD LIOW2 TOF sensor


libqrb5165-io  (0.4.2 to 0.4.5)
0.4.5
    * allow gpio reads on output
    * voxl-gpio improvements
0.4.4
    * fix bug where reading set direction
0.4.3
    * add missing dependency on libmodal-journal


qrb5165-system-tweaks  (0.2.5 to 0.2.6)
0.2.6
    * fix missing M0052 handling


voxl-bind-spektrum  (0.1.0 to 0.1.1)
0.1.1
    * add support for M0154 (GPIO 20)


voxl-camera-calibration  (0.5.3 to 0.5.4)
0.5.4
    * better intrinsic guess for ov9782 tracking cams


voxl-camera-server  (1.8.9 to 1.9.1)
1.9.1
    * liow2 tof
    * new royale support
1.9.0
    * targeted for SDK 1.2.x
    * swap to tof2_data_t type for tof publication to MPA
    * support for new voxl-fsync kernel module for synced cameras
    * temperarily add ar0144_slave drivers
    * new build dependency on voxl-cross:V2.7
    * (qrb5165) requires new system image with royale-5-8-spectre-6-10
1.8.10
    * fix cpu monitor not starting libmodalpipe helper (tof decimator feature)


voxl-configurator  (0.4.8 to 0.5.6)
0.5.6
    * apq bug fix
0.5.5
    * updated D0008-V4 support
0.5.4
    * extend transmitter handling
    * add ghost rc support
0.5.3
    * extend error handling
0.5.2
    * add experimental mode handling
    * add voxl-configure-open-vins framework
    * kill voxl-mavlink-server before setting px4 params
0.5.1
    * add support for fpv revb v4
    * remove px4 params file before seting up voxl-px4 to ensure no old params survive
0.5.0
    * fix D0013 board type
0.4.9
    * new cal file check for dual ar0144 cams


voxl-cpu-monitor  (0.4.7 to 0.4.8)
0.4.8
    * add perf mode config option


voxl-docker-support  (1.3.0 to 1.3.1)
1.3.1
    * fix apq missing system.slice error


voxl-esc  (1.3.7 to 1.4.0)
1.4.0
    * fix fpv m0138 params
1.3.9
    * add support for fpv v4
    * add "setup_starling_silver_motors" and "black motors" options
    * set cpu performance mode during firmware flash
1.3.8
    * put fpvrevB back to silent (no) startup chirp


voxl-logger  (0.3.5 to 0.4.0)
0.4.0
    * voxl-replay start and stop time options
    * tof2_data_t type support
0.3.12
    * fixed typo in info/json
0.3.11
    * added support for tof2 data type with automatic detection
    * added support for mavlink messages
0.3.10
    * fixed include/exclude bug
0.3.9
    * replaced timeout with -s/--start -f/--finish times
    * .clang-format added
0.3.8
    * added -t/--timeout functionality to voxl-replay
0.3.7
    * added population of camera info based on .csv values
0.3.6
    * added replay support for RAW16 images
    * added option to only replay certain pipes from log with -e option


voxl-mavlink-server  (1.3.2 to 1.4.0)
1.4.0
    * add mission delay feature
    * make detecting of autopilot sysid more robust
    * fix bug where autopilot reporting unix time of 0 breaks linux system time


voxl-modem  (1.0.8 to 1.0.9)
1.0.9
    * fix bug in microhard logic


voxl-mpa-tools  (1.1.3 to 1.1.5)
1.1.5
    * update to use new tof2_data_t type
    * add more data to voxl-inspect-tof
1.1.4
    * update FPV tracking extrinsics for new mount
    * add a downward tracking extrinsic for experimentation


voxl-portal  (0.6.3 to 0.6.5)
0.6.5
    * new page to download systemd service journals
    * updates to health check
0.6.4
    * migrate to new tof2_data_t type
    * auto restart service on crash


voxl-px4-params  (0.3.3 to 0.3.8)
0.3.8
    * update D0013 params
0.3.7
    * add D0008-V4 param file
0.3.6
    * add experimental section
0.3.5
    * D0008 to Lithium Ion battery
0.3.4
    * test and validate param reset option
    * make param uploading more robust


voxl-px4  (1.14.0-2.0.63 to 1.14.0-2.0.68)
1.14.0-2.0.68
    * A couple of voxl2_io driver updates to improve initial protocol check and calibration
1.14.0-2.0.67
    * Support for VIO waypoint missions without GPS. With or without magnetometer.
    * Miscellaneous HITL infrastructure improvements
1.14.0-2.0.66
    * Improved the timeout mechanism on the muorb aggregator to avoid unneeded data sends
    * Added the ORB queue length to the meta data so that it can be properly set by the UORB COMMUNICATOR interface code
    * Fixed missing queue length adjustment from remote topics
    * Added param client / server status values
    * Fixed a uORB / muorb subscription issue that was causing duplication of topic publication data
    * Added some useful statistics and status reporting to the muorb modules
    * add params and set flags in case of ESC warn and over temperature events (#53)
1.14.0-2.0.65
    * Updates to the voxl2_io version check to allow retries with eventual failure
1.14.0-2.0.64
    * 60k RPM support in voxl-esc driver
    * HITL parameters to control sending of sensor_gps and sensor_mag
    * GPS UBX driver changes for baudrate ordering, BBR writes, and retries to help with M10
    * Move time offset to apps side so that DSP is now the timing center.


voxl-qvio-server  (1.0.0 to 1.0.2)
1.0.2
    * expose the image mask feature in the config file
1.0.1
    * allow second camera of a stereo pair to be used
    * extend low-feature init timeout to 3s


voxl-tflite-server  (0.3.1 to 0.3.2)
0.3.2
    * add dynamic reading for number of yolo classes


voxl-utils  (1.3.8 to 1.3.9)
1.3.9
    * add libfc_sensor to voxl-version


voxl-vision-hub  (1.7.3 to 1.7.4)
1.7.4
    * update to use the new tof2_data_t type for TOF VOA
```

# SDK 1.2.X Test Matrix

| Symbol                                                    | Definition              |
|-----------------------------------------------------------|-------------------------|
| <img src="/icons/circle-check-big.svg">                   | Passed                  |
| <img src="/icons/loader-circle.svg">                      | In Progress             |
| <img src="/icons/circle-alert.svg">                       | Failed                  |
| <img src="/icons/link-2-off.svg">                         | Not Supported           |

## SDK 1.2.0 Test Status

| Architecture             | Description                                              | Indoor Test Status                                     | Outdoor Test Status                                       |
|--------------------------|----------------------------------------------------------|--------------------------------------------------------|-----------------------------------------------------------|
| D0001                    | [m500](/m500/)                                           | <img src="/icons/circle-check-big.svg">                | <img src="/icons/circle-check-big.svg">                   |
| D0003                    | Seeker                                                   | <img src="/icons/circle-check-big.svg">                | <img src="/icons/circle-check-big.svg">                   |
| D0004                    | [Qualcomm Flight RB5 5G](/qualcommflightrb5/) Deprecated | <img src="/icons/link-2-off.svg">                      | <img src="/icons/link-2-off.svg">                         |
| [D0005-4](/voxl2-d0005/) | [Starling](/starling/)                                   | <img src="/icons/circle-check-big.svg">                | <img src="/icons/loader-circle.svg">                      |
| [D0006-4](/voxl2-d0006/) | [Sentinel](/sentinel/)                                   | <img src="/icons/circle-check-big.svg">                | <img src="/icons/loader-circle.svg">                      |
| D0011-4                  | PX4 Autonomy Dev Kit, see: [Starling](/starling/)        | <img src="/icons/circle-check-big.svg">                | <img src="/icons/loader-circle.svg">                      |
| D0012-4                  | Starling 2 Max, see: [Starling](/starling/)              | <img src="/icons/link-2-off.svg">                      | <img src="/icons/link-2-off.svg">                         |
| [D0014-4](/voxl2-d0014/) | Starling 2, see: [Starling](/starling/)                  | <img src="/icons/link-2-off.svg">                      | <img src="/icons/link-2-off.svg">                         |
| [D0015-4](/voxl2-d0015/) | Fixed Wing (WIP)                                         | <img src="/icons/link-2-off.svg">                      | <img src="/icons/link-2-off.svg">                         |
