---
layout: default
title: VOXL 2 Coax Camera Bundles
parent: VOXL 2 Dev Bundles
nav_order: 5
has_children: false
permalink: /voxl2-coax-camera-bundles/
---

# VOXL 2 Coax Camera Bundles
{: .no_toc }



## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

| Part Number                                   |Description                       |Purchase Link                              | 
|:---                                           |:---                              |:--                                        |
| `MDK-M0173-1-01 (C26 Camera Config)`          | Dual Tracking, Hires, PMD TOF    | [Buy Here](https://modalai.com/m0173-c26) |
| `MDK-M0173-1-02 (C27 Camera Config)`          | Triple Tracking, Hires, PMD TOF  | [Buy Here](https://modalai.com/m0173-c27) |
| `MDK-M0173-1-03 (C28 Camera Config)`          | Dual Tracking, Dual Hires        | [Buy Here](https://modalai.com/m0173-c28) |

[M0173 VOXL 2 Link](/M0173/)

## Kit Pictures and Descriptions

### MDK-M0173-1-01
- (1) Starling 2 Image Sensor Front-end Adapter ([M0173-01](https://docs.modalai.com/M0173/))
- (1) IMX412 High Resolution sensor ([M0166-01](https://docs.modalai.com/M0166/)) module
- (2) AR0144 tracking sensor ([M0161-01](https://docs.modalai.com/M0161/)) modules 
- (1) PMD LIOW2 TOF on ([M0178](https://docs.modalai.com/M0178/)) adapter board
- (1) Flex cable for TOF (M0177)
- (3) 80mm coax cables for image sensors

Use Camera Config C26 for this Kit

![MDK-M0173-1-01 C26 Config Kit Config](/images/dev-kits/M0173_C26_Config_updated.png)

### MDK-M0173-1-02
- (1) Starling 2 Image Sensor Front-end Adapter ([M0173-01](https://docs.modalai.com/M0173/))
- (1) IMX412 High Resolution sensor ([M0166-01](https://docs.modalai.com/M0166/)) module
- (3) AR0144 tracking sensor ([M0161-01](https://docs.modalai.com/M0161/)) modules 
- (1) PMD LIOW2 TOF on ([M0178](https://docs.modalai.com/M0178/)) adapter board
- (1) Flex cable for TOF (M0177)
- (4) 80mm coax cables for image sensors

Use Camera Config C27 for this Kit

![MDK-M0173-1-02 C27 Config Kit Config](/images/dev-kits/M0173_C27_Config_updated.png)

### MDK-M0173-1-03
- (1) Starling 2 Image Sensor Front-end Adapter ([M0173-01](https://docs.modalai.com/M0173/))
- (2) IMX412 High Resolution sensor ([M0166-01](https://docs.modalai.com/M0166/)) modules
- (2) AR0144 tracking sensor ([M0161-01](https://docs.modalai.com/M0161/)) modules 
- (4) 80mm coax cables for image sensors

Use Camera Config C28 for this Kit

![MDK-M0173-1-03 C28 Config Kit Config](/images/dev-kits/M0173_C28_Config_updated.png)

## Hardware Setup

Please exercise ESD safety precautions and have electronics knowledge when interfacing with the hardware. We recommend always using grounding wrist straps and ESD mats.

<img src="/images/voxl-developer-bootcamp/m0054-esd-warning.png" alt="m0054-esd-warning"/>

Users should note that the connectors for coaxial cables are less rugged than flex cable connectors, and are only rated for 20 insertions. We recommend only disconnecting and reconnecting coaxial cables when absolutely necessary, and only with the appropriate tool ([Hirose DF-SD3/RE-MD](https://www.mouser.com/ProductDetail/Hirose-Connector/DF-SD3-RE-MD?qs=cbprxTG2Yq8BLNmyfKF1vw%3D%3D)).
For more information on proper handling of ModalAI coaxial cables, please refer to the [Micro-Coaxial User Guide](https://modalai.com//micro-coax-user-guide/).

### Assemble Heat Sink and TOF/Adapter

The TOF adapter module will rapidly overheat when used without a heatsink attached, causing errors. Users who wish to evaluate the TOF without having custom solutions available may use the heatsink and thermal pads provided by ModalAI in their kit. These are found in a small bag under the foam at the bottom of the box.

**1** Carefully disconnect the TOF module from the flex cable connecting it to the M0173 board.

**2** Apply a thermal pad to both front of heat sink centered between the two heat sink holes.

![TOF Heatsink Step 1](/images/dev-kits/TOF_heatsink_step1.png)

**3** Stick the TOF module to the thermal pad on front of heat sink, being CAREFUL to line up the TOF module such that the plastic housing is centered between the two heat sink holes. The flex cable connecting the two sides of the module should also be centered in the slot on the side of the heat sink.

![TOF Heatsink Step 2](/images/dev-kits/TOF_heatsink_step2.png)

**4** Apply a thermal pad to back of heat sink centered between the two heat sink holes.

![TOF Heatsink Step 3](/images/dev-kits/TOF_heatsink_step3.png)

**5** Wrap the TOF adapter and flex cable around and stick the PCB to the back of the heatsink. The Mounting hole in the PCB should be centered in the top heatsink mounting hole. The bottom the the PCB should be centered left/right relative to the mounting hole.

![TOF Heatsink Step 4](/images/dev-kits/TOF_heatsink_step4.png)

**6** Carefully reconnect the TOF module to the flex cable connecting it to the M0173 board, being careful to check connector orientation.

![TOF Heatsink Step 5](/images/dev-kits/TOF_heatsink_step5.png)

## Software Setup

To configure your VOXL 2 SDK to run these camera combinations, we need to ADB or SSH into your VOXL 2 and configure its SKU number to include the correct `C` number.


```Bash
voxl2:/$ voxl-configure-sku 
````


Then you are going to configure the SKU for a VOXL 2 'board only' setup so that MPA does not think any other hardware is attached such as ESCs or other sensors that are included in the drone dev kits.

```
------------------------------------------------------------------
            Welcome to the voxl-configure-sku Wizard!

VOXL is not yet configured for a particular SKU.
Please select the new desired product family:

 1) accept and continue     12) voxlcam
 2) starling-2              13) seeker
 3) starling-2-max          14) rb5-flight
 4) starling                15) flight-deck
 5) px4-autonomy-dev-kit    16) voxl-deck
 6) sentinel                17) voxl2-flight-deck
 7) fpv                     18) voxl2-test-fixture
 8) D0010                   19) voxl2-mini-test-fixture
 9) stinger                 20) voxl2-board-only
10) D0015                   21) voxl2-mini-board-only
11) m500                    22) quit
#? 20
```

Select the correct 'C' number for your kit.

```
If you would like to select a special camera config that differs
from the default for your product family, please select an option.
Simply hit ENTER to use the default

26 - M0173 with Tof imx412 and dual AR0144
27 - M0173 with Tof imx412 and triple AR0144
28 - M0173 with Dual IMX412 and Dual AR0144
29 - M0173 with TOF Dual IMX412 and Dual AR0144
30 - M0173 with dual TOF Dual IMX412 and Dual AR0144
 C - use user-defined custom camera config in
     (/data/modalai/custom_camera_config.txt)
 q - Quit The Wizard


Simply hit ENTER to use the default

selection: 28
```

Then run voxl-configure-mpa and reboot. This will load the appropritate camera driver binaries and configure voxl-camera-server to start the sensors on boot.

```
voxl2:/$ voxl-configure-mpa
```

After a reboot, all camera sensors should slow up when running voxl-inspect-cam -a

```
voxl2:/$ voxl-inspect-cam -a
```

```
|                 Pipe Name |  bytes  | wide |  hgt |exp(ms)| gain | frame id |latency(ms)|  fps |  mbps  | format
|    hires_down_large_color |18495360 | 4056 | 3040 | 33.23 |  806 |       54 |    339.0  | 29.4 | 4346.9 | NV12
|  hires_down_large_encoded |     379 | 4056 | 3040 | 33.23 |  806 |       55 |    350.9  | 28.9 |    0.5 | H265 (P)   
|     hires_down_large_grey |12330240 | 4056 | 3040 | 33.23 |  806 |       56 |    310.7  | 29.4 | 2900.1 | RAW8
|    hires_down_small_color | 1179648 | 1024 |  768 | 33.23 |  806 |       57 |    298.9  | 30.0 |  282.7 | NV12
|  hires_down_small_encoded |      72 | 1024 |  768 | 33.23 |  806 |       57 |    292.0  | 29.4 |    0.1 | H265 (P)   
|     hires_down_small_grey |  786432 | 1024 |  768 | 33.23 |  806 |       57 |    295.7  | 30.0 |  188.5 | RAW8
|       hires_down_snapshot |
|   hires_front_large_color |18495360 | 4056 | 3040 | 18.06 |  788 |       58 |    322.0  | 29.2 | 4326.4 | NV12
| hires_front_large_encoded |     472 | 4056 | 3040 | 18.06 |  788 |       59 |    340.9  | 28.8 |    0.9 | H265 (P)   
|    hires_front_large_grey |12330240 | 4056 | 3040 | 18.06 |  788 |       60 |    298.1  | 29.3 | 2886.9 | RAW8
|   hires_front_small_color | 1179648 | 1024 |  768 | 18.06 |  788 |       60 |    280.9  | 30.3 |  285.9 | NV12
| hires_front_small_encoded |      99 | 1024 |  768 | 18.06 |  788 |       60 |    279.8  | 29.8 |    0.5 | H265 (P)   
|    hires_front_small_grey |  786432 | 1024 |  768 | 18.06 |  788 |       61 |    278.7  | 30.3 |  190.6 | RAW8
|      hires_front_snapshot |
|              qvio_overlay | 1146880 | 1280 |  896 |  5.45 |  266 |   137368 |     26.8  | 30.0 |  275.3 | RAW8
|             tracking_down | 1024000 | 1280 |  800 |  5.75 |  363 |   137347 |     23.1  | 30.0 |  245.8 | RAW8
|            tracking_front | 1024000 | 1280 |  800 |  5.45 |  266 |   137368 |     24.7  | 30.0 |  245.8 | RAW8
```


