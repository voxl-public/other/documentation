---
layout: default
title: Sentinel User Guide
parent: Sentinel
nav_order: 5
has_children: true
permalink: /sentinel-user-guides/
---

# Sentinel Reference Drone User Guide
{: .no_toc }


This guide will walk you from taking Sentinel out of the box and up into the air!

For technical details, see the [datasheets](/sentinel-datasheet/).

![Sentinel](/images/sentinel/sentinel-front-clear.png)

[First Step: Unboxing](/sentinel-user-guide-unboxing/){: .btn .btn-green}
