---
layout: default
title: Sentinel Power Supply and Battery
parent: Sentinel User Guide
nav_order: 10
has_children: false
permalink: /sentinel-user-guide-power/
---

# Sentinel Power Supply and Battery
{: .no_toc }

1. TOC
{:toc}	

## Safety First!

For setup and configuration of your Sentinel you must **Remove the Propellers** for safety.


## Battery Information

Sentinel requires a Gens Ace 5000mAh, or any 3S battery with XT60 connector.

Buy Battery [Here](https://www.modalai.com/products/gens-ace-11-1v-60c-3s-5000mah-lipo-battery-pack-with-xt60-plug) 

![vehicle-battery-connected](/images/sentinel/sentinel-unplugged.jpg)

To install a 3s battery, slide into the body as shown. There is a stop at the front of the vehicle that will prevent the battery from sliding too far forward. The battery should be pressed all the way in until it hits the stop for a consistent center of mass. 

![vehicle-battery-connected](/images/sentinel/sentinel-powered.jpg)

Power on Sentinel by connecting the battery to the XT60 connector. 

## Benchtop Power Supply

For desktop use, a 12VDC wall power supply can be used and is [available here](https://www.modalai.com/products/ps-xt60).  this is convenient when doing development.


[Next Step: Connect over ADB](/sentinel-user-guide-connect-adb/){: .btn .btn-green }
