---
layout: default
title: Seeker RC Settings
parent: Seeker User Guide
nav_order: 30
has_children: false
permalink: /seeker-user-guide-px4/
---

# Seeker RC Radio Setup
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}


## RC Radio Setup

Now you need to bind and calibrate your DSM RC radio through the qGroundControl interface.

These instructions focus on the default receiver using Spektrum DSMx. For other radio types see the [Flight Core Radios Page](https://docs.modalai.com/flight-core-radios/) and [Flight Core Connections Page](/flight-core-connections/#rc-receiver) for more details.

### Bind to Radio

#### Spektrum (Default)

To bind a Spektrum Radio to Seeker, go into the 'Radio' tab under the QGC settings. Click on 'Spektrum Bind', select the type of transmitter you are using, then click 'OK'. 

![spek-bind](/images/seeker/spek-bind-1.png)

Once selected, the orange light on Seeker's receiver will start flashing (See Below). Go ahead and bind your transmitter to the drone by doing a regular Spektrum bind. The exact instructions will depend on the specific transmitter you are using, so make sure to check Spektrum's website or your user manual for the proper steps. 

More detailed instructions about Spektrum binding can be found [here](https://docs.px4.io/master/en/config/radio.html).


![spek-bind-2](/images/seeker/spek-bind-2.gif)


#### FrSky
Video tutorial on how to pair an FrSky X8R [(Youtube)](https://www.youtube.com/watch?v=1IYg5mQdLVI)

### Calibrate Radio

Now follow the on-screen instructions to calibrate the range and trims of your radio.

![8-calibrate-radio.png](/images/voxl-sdk/qgc/8-calibrate-radio.png)

### Confirm RC Settings

Obviously every user will want to use different flight modes and different switch assignments, but for getting started with VOXL and and Flight Core we suggest starting with something similar to this configuration and working from there.

- "Flap Gyro" switch left of Spektrum Logo
  - Channel 6
    - Up position:     Manual Flight Mode
    - Middle Position: Position Flight Mode
    - Down Position:   Offboard Flight mode

- "Aux2 Gov" switch right of Spektrum Logo
  - Channel 7
    - Up position:     Motor Kill Switch Engaged
    - Down Position:   Motor Kill Switch Disengaged (required to fly)

Since we have a manual kill switch on the radio there is no need for the "safety switch" on the Pixhawk GPS module as so it is disabled in our params file in favor of the kill switch.

![9-flight-mode-config.png](/images/voxl-sdk/qgc/9-flight-mode-config.png)

If you have a Spektrum DX6e or DX8 radio with a clean acro-mode model you can accomplish the above channel mapping by loading the following config file

[https://gitlab.com/voxl-public/px4-parameters/-/blob/master/params/spektrum_dx8_config.params](https://gitlab.com/voxl-public/px4-parameters/-/blob/master/params/spektrum_dx8_config.params)

Still confirm the mapping in qGroundControl before flight!

---

[Next Step: Pre-Flight](/seeker-user-guide-preflight/){: .btn .btn-green }
