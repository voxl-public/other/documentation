---
layout: default
title: 2.7 M500
parent: 2. Hardware Quickstart
nav_order: 50
has_children: false
permalink: /m500-hardware-quickstart/
---

# M500 Hardware Quickstart
{: .no_toc }

This section contains a hardware quickstart and is intended for developer type users.  It provides the minimum steps needed to get a device powered up and ready to connect to a console.

{: .alert .simple-alert}
**WARNING:** *Unmanned Aerial Systems (drones) are sophisticated, and can be dangerous.  Please use caution when using this or any other drone.  Do not fly in close proximity to people and wear proper eye protection.  Obey local laws and regulations.*

For technical details, see the [datasheet](/m500-datasheet/).

## Quickstart Video

{% include youtubePlayer.html id="gTuSaHLCz8w" %}

## How to Power On and Prepare to Connect

### 1. Ensure propellors are removed

![m500-out-of-box](/images/m500/m500-out-of-box.jpg)

### 2. Connect Micro USB Cable

- Connect Micro USB cable to the M500 (labeled `USB for VOXL` below) 
- Connect other side of USB cable (recommmended USB type A) to host computer for later use

![vehicle-overview-right-144-1024.jpg](/images/m500/vehicle-overview-right-144-1024.jpg)

### 3. Power ON

- Connect the battery or wall power supply to the XT30 connector at the rear of the vehicle
  - *NOTE: do not arm the vehicle while using wall power*

## How to Power Off

Pulling the power is okay in normal operation. If you are logged into VOXL and have been modifying files on the file system then it's recommended to run `sync` to make sure your changes are flushed to flash memory before pulling power.

Do not pull power while flashing software as this can brick the device.

[Next Step: Set up ADB](/setting-up-adb/){: .btn .btn-green }
