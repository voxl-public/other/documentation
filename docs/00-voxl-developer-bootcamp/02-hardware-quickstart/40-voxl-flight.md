---
layout: default
title: 2.5 VOXL Flight
nav_order: 40
parent: 2. Hardware Quickstart
has_children: false
permalink: /voxl-flight-hardware-quickstart/
has_toc: true
---

# VOXL Flight Hardware Quickstart
{: .no_toc }

This section contains a hardware quickstart and is intended for developer type users.  It provides the minimum steps needed to get a device powered up and ready to connect to a console.

{: .alert .simple-alert}
**NOTE**:  *Please exercise ESD safety precautions and have electronics knowledge when interfacing with the hardware.*

For technical details, see the [datasheet](/voxl-flight-datasheet/).

## Quickstart Video

<div class="video-container">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/Kjw7X6WKDUw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>

## How to Setup Hardware

### How to Power On and Prepare to Connect

<br>
**NOTE**:  Please exercise ESD safety precautions and have electronics knowledge when interfacing with the hardware.

- Connect Micro B side of USB cable to VOXL Flight
- Connect other side of USB cable (recommmended USB type A) to host computer for later use

![VOXL Flight HW Setup](/images/voxl-flight/m0019-bootcamp-quickstarts.jpg)

- Connect the power cable to `Power` as shown
- Connect the other side of the power cable to the VOXL Power Module as shown
- Connect one side of the USB cable to the host computer and connect the other side to the `USB (adb)` port as shown.
  - **Note:** the Micro USB connector will consume only a portion of the USB 3.0 port.  A USB 3.0 cable can be used as well.
- Now plug in the power supply (**5V/6A DC output**) to the wall, and plug it in to the VOXL Power Module barrel jack as shown

## How to Power Off

Pulling the power is okay in normal operation. If you are logged into VOXL and have been modifying files on the file system then it's recommended to run `sync` to make sure your changes are flushed to flash memory before pulling power.

Do not pull power while flashing software as this can brick the device.

[Next Step: Set up ADB](/setting-up-adb/){: .btn .btn-green }
