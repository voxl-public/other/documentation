---
layout: default
title: 16 voxl-cross-template
parent: VOXL Developer Bootcamp
nav_order: 16
permalink: /voxl-cross-template/
---

# Using VOXL Docker and VOXL Cross to Build and Deploy Applications
{: .no_toc }
---

This page will walk you through how to use `voxl-docker` and the `voxl-cross` docker image to build and deploy an application onto VOXL. We will use [voxl-cross-template](https://gitlab.com/voxl-public/voxl-sdk/voxl-cross-template/) as an example. This example simply prints IMU data from VOXL's onboard sensors to a shell. 

Make sure you have [Installed VOXL Docker and VOXL Cross](/voxl-docker-and-cross-installation/) first.

We encourage you to follow along!!!

## Background
All ModalAI projects contain these 4 scripts in the root dir:

**clean.sh:** cleans up build files and .ipk/.deb packages.

**build.sh:** builds project without cleaning first; may specify platform target with the following options where appropriate (`apq8096`, `qrb5165`, `native`).

**make_package.sh:** makes .ipk and/or .deb packages after building; should take an argument `ipk` or `deb` to specify which package to build.

**deploy_to_voxl.sh:** push the just-generated ipk or deb to VOXL and install.

The general workflow is clean, build, make, deploy.

## Build Instructions

First, clone the voxl-cross-template repository and cd into it:
```
me@mylaptop:~/$ git clone https://gitlab.com/voxl-public/voxl-sdk/voxl-cross-template.git
me@mylaptop:~/$ cd voxl-cross-template
```

1) Launch the `voxl-cross` docker. After it launches, your terminal pre-cursor should change to `voxl-cross:~/$`:

```bash
me@mylaptop:~/voxl-cross-template$ voxl-docker -i voxl-cross
voxl-cross:~$
```

2) Install build dependencies inside the docker. You must specify both the hardware platform and binary repo section to pull from. The hardware should be either `qrb5165` (the processor VOXL 2 uses) or `apq8096` (the processor VOXL and VOXL Flight uses). The binary repo must be either `dev`, `stable` or `staging`. See [here](/configure-pkg-manager/) for more information.

```bash
voxl-cross:~$ ./install_build_deps.sh qrb5165 dev
```

3) Build scripts should take the hardware platform as an argument: `qrb5165` or `apq8096`. 

```bash
voxl-cross:~$ ./build.sh qrb5165
OR
voxl-cross:~$ ./build.sh apq8096
```

4) Make an ipk or deb package while still inside the docker.

```bash
voxl-cross:~$ ./make_package.sh deb
OR
voxl-cross:~$ ./make_package.sh ipk
```

This will make a new package (.deb or .ipk) file in your working directory. The name and version number came from the package control file. If you are updating the package version, edit it there.

Optionally add the `--timestamp` argument to append the current data and time to the package version number in the debian package.

## Deploy to VOXL

You can now push the ipk or deb package to VOXL and install with dpkg/opkg however you like. To do this over ADB, you may use the included helper script: `deploy_to_voxl.sh`. Do this outside of docker as your docker image probably doesn't have usb permissions for ADB.

The `deploy_to_voxl.sh` script will query VOXL over adb to see if it has dpkg installed. If it does then then .deb package will be pushed an installed. Otherwise the .ipk package will be installed with opkg.

```bash
(outside of docker)
me@mylaptop:~/voxl-cross-template$ ./deploy_to_voxl.sh
```

This deploy script can also push over a network given sshpass is installed and the VOXL uses the default root password.


```bash
(outside of docker)
me@mylaptop:~/voxl-cross-template$ ./deploy_to_voxl.sh ssh 192.168.1.123
```

## Running the Deployed Program

To see your now built and deployed application running on the VOXL, ADB or ssh into your VOXL. In your VOXL's bash shell, run:
```
voxl2:/$ hello-cross
```
Try arguments `-h` (for a help menu), `-i` (to provide an IMU input), and `-n` (to add a newline after every IMU output). If you add a `-i` parameter followed by the name of an IMU (press tab after typing `-i` to see available IMU's), you should see IMU data being printed to the shell.

You can see that the name of the command to run in the shell is defined in /src/CMakeLists.txt:
```
SET(TARGET hello-cross)
```

## Next Steps

This section of the bootcamp has been a short intro to building applications using `voxl-cross`. This process will suffice for 90% of applications, however, there are three other docker images that may need to be used instead of `voxl-cross` in unique circumstances. In addition, writing custom applications has been out of the scope of this Bootcamp.



<br>
[Next: Bootcamp Graduation](/bootcamp-graduation/){: .btn .btn-green }
