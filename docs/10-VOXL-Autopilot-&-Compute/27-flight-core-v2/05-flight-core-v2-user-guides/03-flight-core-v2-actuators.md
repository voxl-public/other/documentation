---
layout: default
title: Flight Core v2 Actuators
parent: Flight Core v2 User Guides
grand_parent: Flight Core v2
nav_order: 3
permalink: /flight-core-v2-actuators/
---

# Flight Core v2 Actuators
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Hardware

### ModalAI UART ESC

UART5 is exposed on `J5`, which is mapped to `TELEM2` in PX4 and available to Nuttx as `/dev/ttyS4`.

This is the default port used by the ModalAI UART ESC Driver when enabled. 

**PLEASE NOTE** in `1.13.2-0.1.1` and older, the recommended port is J1.  It was changed to J5 starting `1.13.2-0.1.2` and newer.

Recommended is [MCBL-00063](/cable-datasheets/#mcbl-00063/) (6p JST ModalAI UART ESC Cable) for this interface.

![Flight Core v2](/images/flight-core-v2/m0087-modalai-esc-v2.png)

**Flight Core v2 J5**

| Pin # | Signal Name                                                                     |
|-------|---------------------------------------------------------------------------------|
| 1     | NC                                                                              |
| 2     | Flight Core v2 Tx, 3P3V (connect to ESC Rx)                                     |
| 2     | Flight Core v2 Rx, 3P3V (connect to ESC Tx)                                     |
| 4     | NC                                                                              |
| 5     | NC                                                                              |
| 6     | GND                                                                             |

**ModalAI ESC J2**

M0049 based hardware `J2`:

| Pin # | Signal Name                                                                     |
|-------|---------------------------------------------------------------------------------|
| 1     | NC                                                                              |
| 2     | ESC Rx                                                                          |
| 2     | ESC Tx                                                                          |
| 4     | NC                                                                              |
| 5     | GND                                                                             |
| 6     | NC                                                                              |

### PWM/DShot

8 PWM channels exposed on `J7`:

| Pin # | Signal Name                                                            |
|-------|------------------------------------------------------------------------|
| 1     | 5VDC (other pins are 3.3V, input or output if supplied at another pin) |
| 2     | PWM_CH1                                                                |
| 3     | PWM_CH2                                                                |
| 4     | PWM_CH3                                                                |
| 5     | PWM_CH4                                                                |
| 6     | PWM_CH5                                                                |
| 7     | PWM_CH6                                                                |
| 8     | PWM_CH7                                                                |
| 9     | PWM_CH8                                                                |
| 10    | GND                                                                    |

## Software

### ModalAI UART ESC Driver Source

An open source PX4 driver is available at [src/drivers/actuators/modalai_esc](https://github.com/PX4/PX4-Autopilot/tree/main/src/drivers/actuators/modalai_esc).

### PWM/DShot

Standard PX4 drivers used.
