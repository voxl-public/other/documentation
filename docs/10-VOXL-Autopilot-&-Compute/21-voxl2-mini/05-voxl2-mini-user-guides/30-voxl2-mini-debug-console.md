---
layout: default
title: VOXL 2 Mini Debug Console
parent: VOXL 2 Mini User Guides
nav_order: 30
permalink:  /voxl2-mini-debug-console/
---

# VOXL 2 Mini Debug Console
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Summary

This is NOT enabled by default as it slows the bootup time down dramatically.  The 2 wire UART console exposed by J4 provides the kernel output (dmesg) and provides a root login to Ubuntu (password `oelinux`).

To enable this, you need to build the debug kernel, see [the guide here](/voxl2-kernel-build-guide/).

Shown below is an example hardware setup.

<img src="/images/voxl2-mini/m0104-j4-debug-console.jpg" alt="m0104-j4-debug-console.jpg" width="1280"/>
