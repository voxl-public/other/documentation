---
layout: default
title: VOXL 2 Mini External Flight Controller
parent: VOXL 2 Mini User Guides
nav_order: 16
permalink:  /voxl2-mini-external-flight-controller/
---

# VOXL 2 Mini Offboard Sensors
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

Please refer to [VOXL2 External Flight Controller](/voxl2-external-flight-controller/).