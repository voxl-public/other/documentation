---
layout: default2
title: Expansion Boards
nav_order: 70
has_children: true
permalink: /expansion-boards/
---

# Expansion Boards
{: .no_toc }

Documentation for ModalAI's range of expansion boards for VOXL and VOXL 2 products.


