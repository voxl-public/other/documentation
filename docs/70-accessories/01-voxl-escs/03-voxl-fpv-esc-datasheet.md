---
layout: default
title: VOXL FPV ESC 4-in-1 Datasheet
parent: VOXL ESCs
nav_order: 3
permalink: /voxl-fpv-esc-datasheet/
---

# VOXL FPV Racing 4-in-1 ESC Datasheet
{: .no_toc }

---

## Hardware Overview

This M0138-1 high-performance ESC uses 100A 40V MOSFETs and board layout optimized for demanding FPV racing applications.
The ESC supports 100A burst current per channel and over 300A total burst current (usually limited by battery and connectors).
Communicate reliably with low-latency digital UART from your flight controller to the 4-in-1 ESC.

This ESC has a dedicated 5V regulated power output to power [VOXL 2](/voxl-2/) or [Flight Core v2](/flight-core-v2/)  with battery voltage and current monitoring support in PX4. Additionally the ESC can power a video transmitter and has the ability to toggle on and off.

![m0138_diagram.jpg](/images/modal-esc/m0138/m0138_diagram.jpg)

## Dimensions

![m0138-dimensions.png](/images/modal-esc/m0138/m0138-dimensions.png)

[M0138 VOXL Racing 4-in-1 ESC 3D CAD](https://storage.googleapis.com/modalai_public/modal_drawings/M0138_RACING_ESC_4_IN_1_REVA.step)
- Board dimensions: 45.5mm x 59.0mm
- Mounting hole pattern: 30.5mm x 30.5mm, M3 (3.05mm)

## Specifications

| Feature                | Details                                                                                         |
|------------------------|-------------------------------------------------------------------------------------------------|
| Power Input            | 2-6S Lipo (6-26V)                                                                               |
|                        |                                                                                                 |
| VOXL Power Output      | 5V @ 6A                                                                                         |
| VTX Power Output       | 16.8V @ 500mA (switch on/off). Passthrough if Vbatt < 16.8                                      |
| AUX Power Output       | 3.3V / 5.0V @ 500mA (switch on/off, voltage selectable in software)                             |
| Payload Connector      | Battery Voltage, JST Connector, rated 4A                                                        |
|                        |                                                                                                 |
| Performance            | 40V, 100A+ Mosfets                                                                              |
|                        | 100A peak current per motor (<1s)                                                               |
|                        | 40A continuous current per motor (with sufficient airflow, application dependent)               |
|                        | Peak total board current 300A+ (<1s)                                                            |
|                        |                                                                                                 |
| Software Features      | Open-loop control (set desired % power)                                                         |
|                        | Closed-loop RPM control (set desired RPM), used in PX4 driver                                   |
|                        | Regenerative braking                                                                            |
|                        | Smooth sinusoidal spin-up                                                                       |
|                        | Tone generation using motors                                                                    |
|                        | Real-time RPM, temperature, voltage, current feedback via UART                                  |
| Communications         | Supported by VOXL 2, VOXL 2 Mini, VOXL Flight, VOXL and Flight Core                             |
|                        | Bi-directional UART up to 2Mbit/s (3.3VDC logic-level)                                          |
|                        |                                                                                                 |
| Connectors             | 4-pin JST GH for UART communication                                                             |
|                        | VOXL output: solder pads for 5.0V / 3.8V (depends on board variant)                             |
|                        | Payload connector: JST SM02B-SFHLS-TF(LF)(SN)                                                   |
|                        | VTX power output: solder pads                                                                   |
|                        | AUX power output: solder pads                                                                   |
|                        |                                                                                                 |
| Hardware               | MCU : STM32F051K86 @ 48Mhz, 64KB Flash                                                          |
|                        | Mosfet Driver : MP6531                                                                          |
|                        | Mosfets: TBD (N-channel)                                                                        |
|                        | Current Sensing : 0.25mOhm + INA186 (total current only). 200A max sensing range                |
|                        | ESD Protection : Yes (on UART and PWM I/O)                                                      |
|                        | Temperature Sensing : 4x internal to MCU, 2x external (top and bottom of PCB)                   |
|                        | On-board Status LEDs : Yes                                                                      |
|                        | Weight (no wires) : 17.9g                                                                       |
|                        | Motor Connectors: N/A (solder pads)                                                             |
| PX4 Integration        | Supported in PX4 1.12 and higher                                                                |
|                        | Available [here in developer preview](https://github.com/modalai/px4-firmware/tree/modalai-esc) |
| Resources              | [Manual](/modal-esc-v2-manual/)                                                                 |
|                        | [PX4 Integration User Guide](/modal-esc-px4-user-guide/)                                        |

## Connector Information and Pin-outs
**UART Connector J1**
* Used for UART communication with the ESC
* Connector on board : BM04B-GHS-TBT
* Mating connector : GHR-04V-S
* Pre-crimped wires : (Digikey) AGHGH28K152 or similar
* 3.3V signals (5.0V input is acceptable)

| Pin >      |     1    |    2    |    3    |    4    |
| :------:   | :------: | :-----: | :-----: | :-----: |
| Function > |    N/C   | UART RX (IN) | UART TX (OUT) |   GND   |

## Neopixel LED Support
![m0138_pwm_neopixel.jpg](/images/modal-esc/m0138/m0138_pwm_neopixel.jpg)
- Single Neopixel RGB LED outputs is available, up to 32 LEDs
- Use test point labeled `RGB` in the AUX IO section of the ESC
- The I/O pin is connected to ESC ID0 PB8 (3.3V levels)
- VAUX output provides 3.3V output for the LED array
- ⚠️*Solder carefully to test points and use strain relief to avoid damaging the pads*
- Test tools: [voxl-esc-neopixel-test.py](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-esc/-/blob/master/voxl-esc-tools/voxl-esc-neopixel-test.py)
- Integration with PX4 is done via `voxl_esc` driver and `modal_io_bridge` 

## PWM Inputs / Outputs
- Two independent PWM output pins are available, one pin per ESC channel
- Use test points 0P and 3P (AUX IO section of the ESC)
- PWM pin `0P` is connected to ESC ID0, `3P` -> ESC ID3 (3.3V levels)
- ⚠️*Solder carefully to test points and use strain relief to avoid damaging the pads*
- PWM output is available in firmware in `39.18` or later
- Operating mode of PWM pins
   - PWM input is diabled on this ESC because only two I/O pins are available
   - PWM output can be enabled by sending specific message to the ESC via UART interface
- PWM output mode specifications
   - 3 frequency modes (50hz, 200gz, 400hz)
   - enable or disable timeout (0.5s). If timeout is enabled, PWM output will be disabled (set to low state) after 0.5s of no commands
   - output range 0-2200 us with 0.05us resolution, special value of 2201us will force HIGH state (can be used as GPIO)
- Test tools: [voxl-esc-pwm.py](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-esc/-/blob/dev/voxl-esc-tools/voxl-esc-pwm.py)
- Integration with PX4 is done via `voxl_esc` px4 driver and `modal_io_bridge` . [test app](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/blob/master/tools/voxl-send-esc-pwm-cmd.c)

## GPIO
- Available in ESC firmware `39.18` or later
- Test tools: [voxl-esc-gpio.py](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-esc/-/blob/dev/voxl-esc-tools/voxl-esc-gpio.py)

| Signal Name     | MCU Pin   | Function                                |
|-----------------|-----------|-----------------------------------------|
| VTX_VREG_ENABLE | CH0 PF0   | VTX Power Control                       |
| AUX_VREG_ENABLE | CH0 PA12  | AUX 3.3 / 5.0V Regulator Power Control  |
| AUX_VREG_ADJUST | CH0 PA11  | AUX 3.3 / 5.0V Regulator Voltage Select (disabled)|

## Wiring Diagrams

### M0138-1 FPV ESC with Flight Core v2

![m0138-flight-core-wiring.jpg](/images/modal-esc/m0138/m0138-flight-core-wiring.jpg)
