---
layout: default
title: M0107 IMX412 Low Light Image Sensor Datasheet
parent: Image Sensors
nav_order: 107
has_children: false
permalink: /M0107/
---

# VOXL Hi-res RGB Sensor Datasheet

## Table of Contents
{: .no_toc .text-delta }

1. TOC
{:toc}

![MSU-M0107picture.jpg](/images/other-products/image-sensors/MSU-M0107picture.jpg)

Built in the USA by ModalAI for VOXL® and VOXL 2! A 4k High-resolution Camera based on the Sony Starvis IMX412 sensor. Enable video recording at 1080p30 or 4k30 using this image sensor module. Built in the USA and NDAA '20 Section 848 compliant.

IMX412-AACK is a (Type 1/2.3") 12.3 Mega-pixel CMOS active pixel type stacked image sensor with a square pixel array. R, G and B pigment primary color mosaic filter is employed. It equips an electronic shutter with variable integration time.

This product has a removable and swappable lens. Note: there can be a small touch of glue to hold the focus in place, but you can break that seal to replace the lens.


## Specification

### Hi-resolution, 4k M0107 m12 IMX412 120° FOV ([Buy Here](https://www.modalai.com/products/msu-m0107))



| Specification | Value                                                                                          |
|----------------|------------------------------------------------------------------------------------------------|
| Sensor         | IMX412 [Datasheet](https://www.sony-semicon.com/files/62/flyer_security/IMX412-AACK_Flyer.pdf) |
| Shutter        | Rolling                                                                                        |
| Max Resolution | 7.857 mm (Type 1/2.3) 12.3 Mega-pixel                                                          |
| Framerate      | TBD                                                                                            |
| Lens Mount     | m12                                                                                            |
| Lens Part No.  | 27629F-16MAS-CM                                                                                |
| Focusing Range | TBD                                                                                            |
| Focal Length   | 2.7mm                                                                                          |
| F Number       | TBD                                                                                            |
| Fov(DxHxV)     | 120.4° x 93.5° x 146°                                                                          |
| TV Distortion  | TBD                                                                                            |
| Weight         | 7.5g                                                                                           |
| IR Filter      | Optional                                                                                       |


### Requirements
### Current/Power Consumption

## Technical Drawings   

#### 3D STEP File
[Download](https://storage.googleapis.com/modalai_public/modal_drawings/M0107-m12.STEP)

#### 2D Diagram


## Module Connector Pinout for J2

| Pin # | Signal Name            | Pin # | Signal Name            |
|-------|------------------------|-------|------------------------|
| 1     | GND                    | 2     | GND                    |
| 3     | NC                     | 4     | NC                     |
| 5     | CCI_I2C_SDA0           | 6     | VREG_LVS1A_1P8         |
| 7     | CCI_I2C_SCL0           | 8     | VREG_L3A_1P1           |
| 9     | CAM0_RST0_N            | 10    | CAM_MCLK0_BUFF         |
| 11    | GND                    | 12    | GND                    |
| 13    | MIPI_CSI0_CLK_CONN_P   | 14    | CAM_FLASH              |
| 15    | MIPI_CSI0_CLK_CONN_M   | 16    | CAM_SYNC_0             |
| 17    | MIPI_CSI0_LANE0_CONN_P | 18    | NC                     |
| 19    | MIPI_CSI0_LANE0_CONN_M | 20    | VREG_L22A_2P8          |
| 21    | GND                    | 22    | GND                    |
| 23    | MIPI_CSI0_LANE1_CONN_P | 24    | NC                     |
| 25    | MIPI_CSI0_LANE1_CONN_M | 26    | NC                     |
| 27    | MIPI_CSI0_LANE2_CONN_P | 28    | NC                     |
| 29    | MIPI_CSI0_LANE2_CONN_M | 30    | NC                     |
| 31    | GND                    | 32    | GND                    |
| 33    | MIPI_CSI0_LANE3_CONN_P | 34    | NC                     |
| 35    | MIPI_CSI0_LANE3_CONN_M | 36    | GND                    |

[Module Connector Schematic](https://storage.googleapis.com/modalai_public/modal_drawings/J2moduleConnectorSchematic.pdf)

## VOXL 2 Integration
SDK 1.1.1 and greater

More information [here](/voxl2-camera-configs/)

| Cable              | Adapter Requirements                                      |
|--------------------|-----------------------------------------------------------|
| [M0036-1](/M0036/) | [M00076](/M0076/) or [M0084](/M0084/) or [M0135](/M0135/) |
| [M0074-1](/M0074/) | [M00076](/M0076/) or [M0084](/M0084/) or [M0135](/M0135/) |
| [M0084-1](/M0084/) | None                                                      |


## VOXL SDK
### Camera Server Configuration
### VOXL Portal

## Image Samples for Sensor
### Indoor
### Outdoor

## Hardware Design Guidance