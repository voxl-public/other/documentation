---
layout: default
title: M0166 Tracking Module Datasheet
parent: Image Sensors
nav_order: 166
has_children: false
permalink: /M0166/
---


# VOXL Tracking Sensor Datasheet

## Table of Contents
{: .no_toc .text-delta }

1. TOC
{:toc}

![MSU-M0166picture.jpg](/images/other-products/image-sensors/MSU-M0166picture.jpg)

The M0166 makes a MIPI tracking camera with a coaxial cable connection out of a AR0144 Image Sensor

## Specification

### MSU-M0166-1-01 OnSemi AR0144 

| Specification                   | Value                   |
|---------------------------------|-------------------------|
| Sensor                          | OnSemi AR0144           |
| Shutter                         | Global                  |
| Resolution                      | 1280x800                |
| Lens Size                       | M7                      |
| Fov(DxHxV)                      | 162° x 139° x 84°       |
| Pixel Size                      | 3um                     |
| Effective Focal Length          | 1.385mm                 |
| F/NO                            | 2.4                     |
| Hyperfocal Distance             | 0.274m                  |
| Weight                          | 1.52g                   |
| IR Filter                       | Yes                     |

### Requirements
### Current/Power Consumption

## Technical Drawings   

#### 3D STEP File
#### 2D Diagram


## Module Connector Pinout for J2 ([S-H26-TS-DS-144](https://docs.modalai.com/micro-coax-user-guide/))

| Pin | CAM Group Function                               | Pin |
|-----|--------------------------------------------------|-----|
|  1  | CCI I2C Bus, SDA                                 |     |
|     | 1.2V DVDD LDO                                    |  2  |
|  3  | CCI I2C Bus, SCL                                 |     |
|     | 1.2V DVDD LDO                                    |  4  |
|  5  | System GND                                       |     |
|     | System GND                                       |  6  |
|  7  | CSI CLK P                                        |     |
|     | 1.2V DVDD LDO                                    |  8  |
|  9  | CSI CLK M                                        |     |
|     | 1.8V VDDIO (LDO)                                 | 10  |
| 11  | MIPI CSI High Speed Diff Pair, Data Lane 0, P    |     |
|     | RESET_N Power down signal (Normally Unused)      | 12  |
| 13  |	MIPI CSI High Speed Diff Pair, Data Lane 0, M    |     |
| 14  |	MIPI CSI High Speed Diff Pair, Data Lane 1, P    |     |
|     |	MCLK, Buffered from VOXL 2, 1.8V                 | 15  |
| 16  | MIPI CSI High Speed Diff Pair, Data Lane 1, M    |     |
|     | System GND                                       | 17  |
| 18  |	System GND                                       |     |
|     | Sensor 2.8V AVDD (LDO)                           | 19  |
| 20  | NC                                               |     |
|     | Sensor Sync Signal                               | 21  |
| 22  | NC                                               |     |
|     | System GND                                       | 23  |
| 24  | NC                                               |     |
|     | NC                                               | 25  |
| 26  | NC                                               |     |


## VOXL Integration
SDK 1.3.0 and greater

More information [here](/voxl2-camera-configs/)

## VOXL SDK
### Camera Server Configuration
### VOXL Portal

## Image Samples for Sensor
### Indoor
### Outdoor

## Hardware Design Guidance