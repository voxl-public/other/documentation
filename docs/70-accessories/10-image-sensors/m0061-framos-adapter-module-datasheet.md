---
layout: default
title: M0061 Framos Adapter Module Datasheet
parent: Image Sensors
nav_order: 61
has_children: false
permalink: /M0061/
---

# VOXL Hires Sensor Datasheet

## Table of Contents
{: .no_toc .text-delta }

1. TOC
{:toc}

![MDK-M0061picture.jpg](/images/other-products/image-sensors/MDK-M0061picture.jpg)

Plugs directly into VOXL®! A 4k High-resolution Camera based on the Sony Starvis IMX412 sensor. Enable video recording at 1080p30 or 4k30 using this image sensor module. NDAA '20 Section 848 compliant.

IMX412-AACK is a (Type 1/2.3") 12.3 Mega-pixel CMOS active pixel type stacked image sensor with a square pixel array. R, G and B pigment primary color mosaic filter is employed. It equips an electronic shutter with variable integration time.

This product has a removable and swappable lens. Note: there can be a small touch of glue to hold the focus in place, but you can break that seal to replace the lens.

## Specification

### MDK-M0061-1 m12 IMX412 120° FOV ([Buy Here](https://www.modalai.com/products/mdk-m0061-1))

| Specification | Value                                                                                          |
|----------------|------------------------------------------------------------------------------------------------|
| Sensor         | IMX412 [Datasheet](https://www.sony-semicon.com/files/62/flyer_security/IMX412-AACK_Flyer.pdf) |
| Shutter        | Rolling                                                                                        |
| Max Resolution | 7.857 mm (Type 1/2.3) 12.3 Mega-pixel                                                          |
| Framerate      | TBD                                                                                            |
| Lens Mount     | m12                                                                                            |
| Lens Part No.  | 27629F-16MAS-CM                                                                                |
| Focusing Range | TBD                                                                                            |
| Focal Length   | 2.7mm                                                                                          |
| F Number       | TBD                                                                                            |
| Fov(DxHxV)     | 120.4° x 93.5° x 146°                                                                          |
| TV Distortion  | TBD                                                                                            |
| Weight         | TBD                                                                                            |
| IR Filter      | TBD                                                                                            |

### MDK-M0061-2 m12 IMX678 120° FOV ([Buy Here](https://www.modalai.com/products/mdk-m0061-2))

| Specification | Value                                                                                          |
|----------------|------------------------------------------------------------------------------------------------|
| Sensor         | IMX678 [Datasheet](https://www.framos.com/wp-content/uploads/FSM-IMX678_V1A_Datasheet_v1.0d_Brief.pdf) |
| Shutter        | Rolling                                                                                        |
| Max Resolution | 8.3 Mpx / 3856 x 2180 px                                                                       |
| Framerate      | TBD                                                                                            |
| Lens Mount     | m12                                                                                            |
| Lens Part No.  | SL-HD3125BMP                                                                                   |
| Focusing Range | TBD                                                                                            |
| Focal Length   | 2.7mm                                                                                          |
| F Number       | TBD                                                                                            |
| Fov(DxHxV)     | 120.4° x 93.5° x 146°                                                                          |
| TV Distortion  | TBD                                                                                            |
| Weight         | TBD                                                                                            |
| IR Filter      | TBD                                                                                            |

### Requirements
### Current/Power Consumption

## Technical Drawings   

#### 3D STEP File
[Download](https://storage.googleapis.com/modalai_public/modal_drawings/M0061_CCA_3D.stp)

#### 2D Diagram


## Module Connector Pinout for J2

| Pin # | Signal Name            | Pin # | Signal Name            |
|-------|------------------------|-------|------------------------|
| 1     | GND                    | 2     | GND                    |
| 3     | NC                     | 4     | NC                     |
| 5     | CCI_I2C_SDA0           | 6     | VREG_LVS1A_1P8         |
| 7     | CCI_I2C_SCL0           | 8     | VREG_L3A_1P1           |
| 9     | CAM0_RST0_N            | 10    | CAM_MCLK0_BUFF         |
| 11    | GND                    | 12    | GND                    |
| 13    | MIPI_CSI0_CLK_CONN_P   | 14    | CAM_FLASH              |
| 15    | MIPI_CSI0_CLK_CONN_M   | 16    | CAM_SYNC_0             |
| 17    | MIPI_CSI0_LANE0_CONN_P | 18    | NC                     |
| 19    | MIPI_CSI0_LANE0_CONN_M | 20    | VREG_L22A_2P8          |
| 21    | GND                    | 22    | GND                    |
| 23    | MIPI_CSI0_LANE1_CONN_P | 24    | NC                     |
| 25    | MIPI_CSI0_LANE1_CONN_M | 26    | NC                     |
| 27    | MIPI_CSI0_LANE2_CONN_P | 28    | NC                     |
| 29    | MIPI_CSI0_LANE2_CONN_M | 30    | NC                     |
| 31    | GND                    | 32    | GND                    |
| 33    | MIPI_CSI0_LANE3_CONN_P | 34    | NC                     |
| 35    | MIPI_CSI0_LANE3_CONN_M | 36    | GND                    |

[Module Connector Schematic](https://storage.googleapis.com/modalai_public/modal_drawings/J2moduleConnectorSchematic.pdf)

## VOXL 2 Integration
### IMX678 based M0062-2

For [Platform Release 0.9.5, sys img 1.5.5](/voxl2-system-image/#changelog).

#### Options for J8

<img src="/images/voxl2/m0054-imx678-m0061-2.JPG" alt="m0054-imx678-m0061-2" width="1280"/>

- [M0076](/M0076/) interposer plugs into M0054 J8
- [M0074](/M0074/) flex then connects to the [M0061-2](/M0062/) backpack for the IMX678 module

<img src="/images/voxl2/m0054-imx678-m0061-2-c.JPG" alt="m0054-imx678-m0061-2-c" width="1280"/>

- [M0076](/M0076/) interposer plugs into M0054 J8
- [M0074](/M0074/) flex then connects to the [M0061-2](/M0062/) backpack for the IMX678 module

#### Options for J7

<img src="/images/voxl2/m0054-imx678-m0061-2-b.JPG" alt="m0054-imx678-m0061-2-b" width="1280"/>

- [M0084](/M0084/) flex plugs into M0054 J7
- [M0084](/M0084/) flex then connects to the [M0061-2](/M0062/) backpack for the IMX678 module

### Dual Hires Example

<img src="/images/voxl2/m0054-imx412-imx678.JPG" alt="m0054-imx412-imx678" width="1280"/>


- IMX412 (M0061-1) connected to M0084 flex, plugged into M0054 J7
- IMX678 (M0062-2) connected to M0036 flex connected to M0076 interposer plugged into M0054 J8


More information [here](/voxl2-camera-configs/)


## VOXL SDK
### Camera Server Configuration
### VOXL Portal

## Image Samples for Sensor
### Indoor
### Outdoor

## Hardware Design Guidance