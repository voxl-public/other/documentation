---
layout: default
title: M0188 VOXL 2 Mini Sparrow Imager Adapter
parent: Image Sensor Flex Cables and Adapters
nav_order: 188
has_children: false
nav_exclude: true
search_exclude: true
permalink: /M0188/
---

# M0188 VOXL 2 Mini Sparrow Imager Adapter
{: .no_toc }

---
# Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

# Summary

## Features

M0188 Serves as a breakout for Camera Groups J6 and J7 on VOXL 2 Mini.

- Micro Coax camera connections
  - 4 two lane MIPI-CSI
  - 2 four lane MIPI-CSI
- hardware synchronization
- 2 FLIR Lepton 3.5 sensor interfaces (1 natively, another using an additional cable to route to an extra SPI).
- 8 channel I2C mux (TCA9548AMRGER, address 0x76)

## Hardware Requirements

- Only compatible with [VOXL 2 Mini](/voxl2-mini/)
- Micro-coax Cables [MCBL-00084](https://www.modalai.com/products/mcbl-00084)


## VOXL SDK Requirements

VOXL SDK 1.4.0+ is required.

## Kernel Requirements for M0188

M0188 requires kernel `m0104-1-var00.2-kernel.img`.

When installing SDK, select "D0013" option to install this kernel.

## Dimensions

### 3D Drawings

[3D STEP](https://storage.googleapis.com/modalai_public/modal_drawings/M0188_SPARROW_IMAGER_ADAPTER_REVA_2024-09-12.step)

### 2D Drawings

TODO
<!-- <img src="/images/elrs-v1/m0139-2d.png"/> -->

## Specifications

| Feature          | Details                                                                                                                                                         |
|:-----------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Weight           | TODO g                                                                                                                                                          |
| HxW              | TODO


# Hardware

## Hardware Block Diagram

<img src="/images/m0188/m0188-hw-block-diagram.png">

[View in fullsize](/images/m0188/m0188-hw-block-diagram.png){:target="_blank"}

## Connectors

### Summary

<img src="/images/m0188/m0188-callouts.png">

| Connector      | Description                             |
| ---            |  ---                                    |
| J0             | Tracking Sensor ID0                     |
| J1             | Hires Sensor ID1                        |
| J2             | Tracking Sensor ID2                     |
| J3             | Hires Sensor ID3                        |
| J6             | Tracking Sensor ID6                     |
| J8             | Tracking Sensor ID8                     |

| Connector      | Description                             |
| ---            |  ---                                    |
| P6             | Board to Board - mates to M0104 J6      |
| P7             | Board to Board - mates to M0104 J7      |

| Connector      | Description                             |
| ---            |  ---                                    |
| J4             | Connects to M0104 J10 SPI               |
| J5             | FLIR Leptopn 2                          |
| J10            | FLIR Leptopn 1                          |

| Connector      | Description                             |
| ---            |  ---                                    |
| J01            | TODO                                    |
| J23            | TODO                                    |
| J45            | TODO                                    |
| J67            | TODO                                    |

## Image Sensor Interfaces

<img src="/images/m0188/m0188-image-sensors.png">

- requires kernel `m0104-1-var00.2-kernel.img`

### Notes for Sensors

- supported coax cables 
  - [MCBL-00084](https://www.modalai.com/products/mcbl-00084)
- HW synchronization
  - tracking sensors enabled sync by default using GPIO 109 ([voxl-fsync-mod](https://gitlab.com/voxl-public/system-image-build/meta-voxl2/-/tree/qrb5165-ubun1.0-14.1a/recipes-kernel/voxl-fsync-mod))

### J0 - Tracking Sensor ID0

- supported image sensors
  - [M0161](https://www.modalai.com/products/msu-m0166) - 1280x800 Global Shutter Sensor AR0144
- 2 MIPI lane support, in combo mode with Sensor ID6

### J1 - Hires Sensor ID1

- supported image sensors
  - [M0161](https://www.modalai.com/products/msu-m0161) - 4k High-resolution, Low-light, IMX412
- 4 MIPI lane support

### J2 - Tracking Sensor ID2

- supported image sensors
  - [M0166](https://www.modalai.com/products/msu-m0166) - 1280x800 Global Shutter Sensor AR0144
- 2 MIPI lane support, in combo mode with Sensor ID8

### J3 - Hires Sensor ID3

- supported image sensors
  - [M0161](https://www.modalai.com/products/msu-m0161) - 4k High-resolution, Low-light, IMX412
- 4 MIPI lane support

### J6 - Tracking Sensor ID6

- supported image sensors
  - [M0161](https://www.modalai.com/products/msu-m0166) - 1280x800 Global Shutter Sensor AR0144
- 2 MIPI lane support, in combo mode with Sensor ID0

### J8 - Tracking Sensor ID8

Not supported yet

## FLIR Lepton Interfaces

<img src="/images/m0188/m0188-lepton-v4.png">

### Lepton 1 - J10

TODO PINOUT

### Lepton 2 - J5 (external SPI to M0104 J10)

TODO PINOUT

## I2C Mux Interfaces

### Summary

An 8-channel I2C mux (`TCA9548AMRGER`) is on the `/dev/i2c-0` bus at address 0x76 when mated to VOXL2 Mini.

### J01 - I2C0 and I2C1

TODO PINOUT

### J23 - I2C2 and I2C3

TODO PINOUT

### J45 - I2C4 and I2C5

TODO PINOUT

### J67  - I2C6 and I2C7

TODO PINOUT

# Software

## Linux Users Guide

<img src="/images/m0188/m0188-linux-user-guide.png">

### Lepton 1 - J10

The `/dev/spidev0.0` SPI interface comes through `M0188 P6`, the `/dev/i2c-0` I2C inteface comes though `M0188 P7`.

I2C is shared between Lepton 1 and Lepton 2 and we need to use a channel selector.

For Lepton 1 I2C, you need to use `CHANNEL0` of the TCA9543A device (write `0x01` to address `0x01` on slave device `0x73`).

### Lepton 2 - J5 (external SPI to M0104 J10)

The `/dev/spidev14.0` SPI interface comes through `J4` that connects to M0104 `J10` externally, the `/dev/i2c-0` I2C inteface comes though `P7`.

I2C is shared between Lepton 1 and Lepton 2 and we need to use a channel selector.

For I2C, you can use `CHANNEL1` of the TCA9543A device (write `0x02` to address `0x01` on slave device `0x73`).

## I2C Mux Interfaces

### Summary

An 8-channel I2C mux (`TCA9548AMRGER`) is on the `/dev/i2c-0` bus at address 0x76 when mated to VOXL2 Mini.

### J01 - I2C0 and I2C1

To use `CHANNEL0` of the TCA9548A device (write `0x01` to address `0x01` on slave device `0x76`).
To use `CHANNEL1` of the TCA9548A device (write `0x02` to address `0x01` on slave device `0x76`).

### J23 - I2C2 and I2C3

To use `CHANNEL2` of the TCA9548A device (write `0x04` to address `0x01` on slave device `0x76`).
To use `CHANNEL3` of the TCA9548A device (write `0x08` to address `0x01` on slave device `0x76`).

### J45 - I2C4 and I2C5

To use `CHANNEL4` of the TCA9548A device (write `0x10` to address `0x01` on slave device `0x76`).
To use `CHANNEL5` of the TCA9548A device (write `0x20` to address `0x01` on slave device `0x76`).

### J67  - I2C6 and I2C7

To use `CHANNEL6` of the TCA9548A device (write `0x40` to address `0x01` on slave device `0x76`).
To use `CHANNEL7` of the TCA9548A device (write `0x80` to address `0x01` on slave device `0x76`).