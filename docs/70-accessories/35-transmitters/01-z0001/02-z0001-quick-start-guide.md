---
layout: default
nav_order: 2
has_children: false
permalink: /z0001-quick-start-guide/
summary: Zorro Blue Quick Start Guide
nav_exclude: true
search_exclude: true
---

# Zorro Blue Quick Start Guide
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }
