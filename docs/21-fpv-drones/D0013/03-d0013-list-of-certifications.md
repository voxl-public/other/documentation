---
layout: default
nav_order: 3
has_children: false
permalink: /d0013-list-of-certifications/
summary: D0013 List of Certifications
nav_exclude: true
search_exclude: true
---

# D0013 List of Certifications
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }
